﻿using Qlik.Engine;
using Qlik.Sense.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QlikSenseSDKExtensions
{
    public static class QlikSenseExtensions
    {
        public static ISheet Sheet(this IApp app, string sheetTitle)
        {
            if (string.IsNullOrWhiteSpace(sheetTitle)) throw new ArgumentException("Title can't be empty", "sheetTitle");
            return app.SheetInfo(s => s.Data.Title.Equals(sheetTitle), s => app.GetSheet(s.Info.Id));
        }

        public static string SheetId(this IApp app, string sheetTitle)
        {
            if (string.IsNullOrWhiteSpace(sheetTitle)) throw new ArgumentException("Title can't be empty", "sheetTitle");
            return app.SheetInfo(s => s.Data.Title.Equals(sheetTitle), s => s.Info.Id);
        }

        public static T SheetInfo<T>(this IApp app, Func<ISheetObjectViewListContainer, bool> elementSelector, Func<ISheetObjectViewListContainer, T> valueSelector)
        {
            return app.SheetInfos(elementSelector, valueSelector).First();
        }

        public static T SheetInfoOrDefault<T>(this IApp app, Func<ISheetObjectViewListContainer, bool> elementSelector, Func<ISheetObjectViewListContainer, T> valueSelector)
        {
            return app.SheetInfos(elementSelector, valueSelector).FirstOrDefault();
        }

        public static IEnumerable<T> SheetInfos<T>(this IApp app, Func<ISheetObjectViewListContainer, T> valueSelector)
        {
            return app.GetSheetList().Items.Select(valueSelector);
        }

        public static IEnumerable<T> SheetInfos<T>(this IApp app, Func<ISheetObjectViewListContainer, bool> elementSelector, Func<ISheetObjectViewListContainer, T> valueSelector)
        {
            return app.GetSheetList().Items.Where(elementSelector).Select(valueSelector);
        }

        public static void FieldSelect(this IApp app, string fieldName, params string[] values)
        {
            app.FieldSelect(fieldName, false, false, values);
        }

        public static void FieldSelect(this IApp app, string fieldName, bool toggleMode, params string[] values)
        {
            app.FieldSelect(fieldName, toggleMode, false, values);
        }

        public static void FieldSelect(this IApp app, string fieldName, bool toggleMode, bool softLock, params string[] values)
        {
            using (var field = app.GetAppField(fieldName))
                field.Select(toggleMode, softLock, values);
        }

        public static void FieldSelect(this IApp app, string fieldName, params double[] values)
        {
            app.FieldSelect(fieldName, false, false, values);
        }

        public static void FieldSelect(this IApp app, string fieldName, bool toggleMode, params double[] values)
        {
            app.FieldSelect(fieldName, toggleMode, false, values);
        }

        public static void FieldSelect(this IApp app, string fieldName, bool toggleMode, bool softLock, params double[] values)
        {
            using (var field = app.GetAppField(fieldName))
                field.Select(toggleMode, softLock, values);
        }

        public static void Select(this IAppField appField, params string[] values)
        {
            appField.Select(false, false, StringComparison.CurrentCultureIgnoreCase, values);
        }

        public static void Select(this IAppField appField, bool toggleMode, params string[] values)
        {
            appField.Select(toggleMode, false, StringComparison.CurrentCultureIgnoreCase, values);
        }

        public static void Select(this IAppField appField, bool toggleMode, bool softLock, params string[] values)
        {
            appField.Select(toggleMode, false, StringComparison.CurrentCultureIgnoreCase, values);
        }

        public static void Select(this IAppField appField, bool toggleMode, bool softLock, StringComparison comparisonType, params string[] values)
        {
            if (appField == null) throw new ArgumentNullException("appField");
            var elemNumbers = appField.Rows().Where(row => values.Any(value => value.Equals(row.First().Text, comparisonType))).Select(row => row.First().ElemNumber);
            appField.SelectValues(elemNumbers, toggleMode, softLock);
        }

        public static void Select(this IAppField appField, params double[] values)
        {
            appField.Select(false, false, values);
        }

        public static void Select(this IAppField appField, bool toggleMode, params double[] values)
        {
            appField.Select(toggleMode, false, values);
        }

        public static void Select(this IAppField appField, bool toggleMode, bool softLock, params double[] values)
        {
            if (appField == null) throw new ArgumentNullException("appField");
            var elemNumbers = appField.Rows().Where(row => values.Any(value => value.Equals(row.First().Num))).Select(row => row.First().ElemNumber);
            appField.SelectValues(elemNumbers, toggleMode, softLock);
        }

        public static IEnumerable<NxCellRows> Rows(this IAppField appField)
        {
            return new PagedCellRowsEnumerable(appField);
        }

        class PagedCellRowsEnumerable : IEnumerable<NxCellRows>
        {
            private IAppField _appField;

            public PagedCellRowsEnumerable(IAppField appfield)
            {
                _appField = appfield;
            }

            public IEnumerator<NxCellRows> GetEnumerator()
            {
                return new PagedCellRowsEnumerator(_appField);
            }

            System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            class PagedCellRowsEnumerator : IEnumerator<NxCellRows>
            {
                NxPage[] currentPage;
                NxDataPage currentDataPage;
                int currentPosition;
                IAppField _appField;

                public PagedCellRowsEnumerator(IAppField appField)
                {
                    _appField = appField;
                    currentPage = new[] { Pager.Default };
                }

                public NxCellRows Current
                {
                    get
                    {
                        if (currentDataPage != null)
                            return currentDataPage.Matrix.ElementAt(currentPosition);
                        return null;
                    }
                }

                public void Dispose()
                {
                    currentPage = null;
                    currentDataPage = null;
                    _appField = null;
                }

                object System.Collections.IEnumerator.Current
                {
                    get { return Current; }
                }

                public bool MoveNext()
                {
                    if (currentPosition > currentPage[0].Top + currentPage[0].Height)
                    {
                        currentPage = Pager.Next(currentPage, _appField.Size.cy).ToArray();
                        currentPosition = 0;
                        currentDataPage = null;
                    }
                    if (currentDataPage == null)
                    {
                        currentDataPage = _appField.GetData(currentPage).FirstOrDefault();
                    }
                    return currentDataPage != null && currentDataPage.Area.Height > 0;
                }

                public void Reset()
                {
                    throw new NotImplementedException();
                }
            }
        }
    }

}
